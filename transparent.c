#include <gtk/gtk.h>
#include <gdk/gdkscreen.h>
#include <cairo.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cairorulercolours.h"
#include "keybindings.h"

/*
 * Screen Ruler program using cairo transparancy
 * This code requires a compositing window manager like beryl, or compiz
 * without it the background is black so you could not see what you are measuring
 *
 * FIXME need to link both the windows somehow the Gtk grouping is broken
 * FIXME when KPanel is visible the cross hair goes worng and all measurements are useless
 *          this must be because event->x and y are incorrect!!!!  need to find the REAL screen pos
 * FIXME joining the windows did not work,  generate in cairo a GtkPixelbuf that is 2x screen_width and 2xscreen height
 * TODO investigate some kind of canvas instead of two window objects
 * TODO ability to click through and disable the crosshair
 * TODO zero the co-ordiantes
 * TODO key bindings for
 *  enable
 *  disable
 *  hide rulers
 *  copy location as string
 * TODO eye-candy 
 *    configurable colors
 *    watermark
 *    transparent about box
 *    transparent Help screens on F1
 * TODO comment the code
 * TODO extract overlay to a different file
 *    
 */

static void screen_changed(GtkWidget *widget, GdkScreen *old_screen, gpointer user_data, gboolean doInit);
static gboolean expose(GtkWidget *widget, GdkEventExpose *event, gpointer user_data);
static gboolean crosshair_expose(GtkWidget *widget, GdkEventExpose *event, gpointer user_data);
static void on_clicked(GtkWindow *win, GdkEventButton *event, gpointer user_data);
//unused
//static void draw_horizontal_marker(GtkWindow *widget, int y);
//static void draw_vertical_marker(GtkWindow *window, int x);
static void on_mousemove(GtkWindow *window, GdkEventButton *event, gpointer user_data);
static void on_keypressed(GtkWindow *window, GdkEventKey *event, gpointer user_data);
GtkWidget *get_crosshair_window();
GtkWidget *get_main_window();
void ruler_down();
void ruler_up();
void ruler_top();

static GtkWidget *window;
static GtkWidget *xwindow;

int main(int argc, char **argv)
{
    initDefaultColours();
    /* boilerplate initialization code */
    gtk_init(&argc, &argv);
    

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "CairoRuler");
    g_signal_connect(G_OBJECT(window), "delete-event", gtk_main_quit, NULL);
    /* Tell GTK+ that we want to draw the windows background ourself.
     * If we don't do this then GTK+ will clear the window to the
     * opaque theme default color, which isn't what we want.
     */
    gtk_widget_set_app_paintable(window, TRUE);

    // second window is for the crosshair
    xwindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(xwindow), "CrossHair");
    g_signal_connect(G_OBJECT(xwindow), "delete-event", gtk_main_quit, NULL);
    gtk_widget_set_app_paintable(xwindow, TRUE);

    GtkWindowGroup* group = gtk_window_group_new();
    gtk_window_group_add_window(group, GTK_WINDOW(window));
    gtk_window_group_add_window(group, GTK_WINDOW(xwindow));
    /* We need to handle two events ourself: "expose-event" and "screen-changed".
     *
     * The X server sends us an expose event when the window becomes
     * visible on screen. It means we need to draw the contents.  On a
     * composited desktop expose is normally only sent when the window
     * is put on the screen. On a non-composited desktop it can be
     * sent whenever the window is uncovered by another.
     *
     * The screen-changed event means the display to which we are
     * drawing changed. GTK+ supports migration of running
     * applications between X servers, which might not support the
     * same features, so we need to check each time.
     */
    g_signal_connect(G_OBJECT(window), "expose-event", G_CALLBACK(expose), NULL);
    g_signal_connect(G_OBJECT(window), "screen-changed", G_CALLBACK(screen_changed), NULL);
    g_signal_connect(G_OBJECT(xwindow), "expose-event", G_CALLBACK(crosshair_expose), NULL);
    g_signal_connect(G_OBJECT(xwindow), "screen-changed", G_CALLBACK(screen_changed), NULL);

    gtk_window_set_decorated(GTK_WINDOW(window), FALSE);
    gtk_window_set_decorated(GTK_WINDOW(xwindow), FALSE);


    gtk_widget_add_events(xwindow, GDK_BUTTON_PRESS_MASK);
    gtk_widget_add_events(xwindow, GDK_POINTER_MOTION_MASK);
    gtk_widget_add_events(xwindow, GDK_KEY_PRESS_MASK);

    gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK);
    gtk_widget_add_events(window, GDK_POINTER_MOTION_MASK);
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);

    g_signal_connect(G_OBJECT(xwindow), "button-press-event", G_CALLBACK(on_clicked), NULL);
    g_signal_connect(G_OBJECT(xwindow), "motion-notify-event", G_CALLBACK(on_mousemove), NULL);
    g_signal_connect(G_OBJECT(xwindow), "key-press-event", G_CALLBACK(on_keypressed), NULL);

    g_signal_connect(G_OBJECT(window), "button-press-event", G_CALLBACK(on_clicked), NULL);
    g_signal_connect(G_OBJECT(window), "motion-notify-event", G_CALLBACK(on_mousemove), NULL);
    g_signal_connect(G_OBJECT(window), "key-press-event", G_CALLBACK(on_keypressed), NULL);

    /* initialize for the current display */
    screen_changed(window, NULL, NULL, TRUE);
    screen_changed(xwindow, NULL, NULL, FALSE);

    /* Run the program */
    gtk_widget_show_all(window);
    gtk_widget_show_all(xwindow);
    gtk_main();

    return 0;
}

int screen_width, screen_height;
static int topoffset = 25;  // this should be always % 5 == 0

/* Only some X servers support alpha channels. Always have a fallback */
gboolean supports_alpha = FALSE;
static GdkWindow* drawable;
static GdkWindow* crosshair_drawable;
static cairo_t* cr;

// used to hold click positions of the ruler relative to the GdkScreen
static int click1X;
static int click1Y; // N.B.  click1Y includes topoffset

static int clickBoxStartX;
static int clickBoxStartY;
static gboolean clickBoxStart;

static int clickBoxEndX;
static int clickBoxEndY;
static gboolean clickBoxEnd;


// click 3 clears the first two

static void screen_changed(GtkWidget *widget, GdkScreen *old_screen, gpointer userdata, gboolean doInit)
{

    /* To check if the display supports alpha channels, get the colormap */
    GdkScreen *screen = gtk_widget_get_screen(widget);
    GdkColormap *colormap = gdk_screen_get_rgba_colormap(screen);

    screen_width = gdk_screen_get_width(screen);
    screen_height = gdk_screen_get_height(screen);

    if (!colormap)
    {
        printf("Your screen does not support alpha channels!\n");
        colormap = gdk_screen_get_rgb_colormap(screen);
        supports_alpha = FALSE;
        // TODO exit with GTK popup
    }
    else
    {
        printf("Your screen supports alpha channels!\n");
        supports_alpha = TRUE;
    }

    /* Now we have a colormap appropriate for the screen, use it */
    gtk_widget_set_colormap(widget, colormap);

    if (doInit == TRUE) {
        click1X = C_R_BORDER_WIDTH;
        click1Y = C_R_BORDER_WIDTH + topoffset;
        //gboolean clickBoxStart = FALSE;
        //gboolean clickBoxEnd = FALSE;
        clickBoxStart = FALSE;
        clickBoxEnd = FALSE;
    }
}

static void hide_cursor() 
{
    GdkPixmap* blank = gdk_pixmap_new(crosshair_drawable, 1, 1, 1);
    GdkColor* na = malloc(sizeof(GdkColor));
    na->red=0;
    na->green=0;
    na->blue=0;
    na->pixel=1;
    GdkCursor* cursor;
    cursor = gdk_cursor_new_from_pixmap(blank, blank, na, na, 1, 1);
    gdk_window_set_cursor(crosshair_drawable, cursor);
    gdk_cursor_destroy(cursor);
    free(na);
}
// top Ruler, vertical as in the ticks themselves are vertical
static void draw_vertical_ticks(cairo_t* cr)
{
    int x = 0;
    int big = C_R_BORDER_WIDTH / 2;
    int little = C_R_BORDER_WIDTH / 4;
    int line_height = big;

    // start set up text
    cairo_text_extents_t te;
    cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
                               CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cr, C_R_RULER_FONT_SIZE);
    char number[10];
    // end set up text

    cairo_save(cr);
    cairo_set_source_rgba(cr, C_R_COLOUR_TICK_R, C_R_COLOUR_TICK_G, C_R_COLOUR_TICK_B, C_R_COLOUR_TICK_A);
    cairo_set_line_width(cr, 0.5);
    
    for(x = click1X; x < screen_width; x += 5) {
        line_height = little;
        if ( (x - click1X) % 10 == 0) {
            line_height = big;
        }
        if ( (x - click1X) % 50 == 0) {
            line_height = big + 5;
        }
        cairo_move_to(cr, x + 0.5, C_R_BORDER_WIDTH + topoffset);
        cairo_line_to(cr, x + 0.5, C_R_BORDER_WIDTH - line_height + topoffset);
        cairo_stroke(cr);
        // start draw numbers
        if ( (x - click1X) % 50 == 0) {
            cairo_set_source_rgba(cr, 0, 0, 0, 0.5);
            sprintf(number, "%d", x - click1X);
            cairo_text_extents(cr, number, &te);
            int textX = x - te.width / 2;
            textX = textX < C_R_BORDER_WIDTH  ? C_R_BORDER_WIDTH : textX;
            cairo_move_to(cr, textX, topoffset + te.height + 5);
            cairo_show_text(cr, number);
            cairo_stroke (cr);
            cairo_set_source_rgba(cr, C_R_COLOUR_TICK_R, C_R_COLOUR_TICK_G, C_R_COLOUR_TICK_B, C_R_COLOUR_TICK_A);
        }
        // end draw numbers
    }

    cairo_restore(cr);
}

// side ruler
static void draw_horizontal_ticks(cairo_t* cr)
{
    int y = 0;
    int big = C_R_BORDER_WIDTH / 2;
    int little = C_R_BORDER_WIDTH / 4;
    int line_height = big;

    // start set up text
    cairo_text_extents_t te;
    cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
                               CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cr, C_R_RULER_FONT_SIZE);
    char number[10];
    // end set up text

    cairo_save(cr);
    cairo_set_source_rgba(cr, C_R_COLOUR_TICK_R, C_R_COLOUR_TICK_G, C_R_COLOUR_TICK_B, C_R_COLOUR_TICK_A);
    cairo_set_line_width(cr, 0.5);
    
    for(y = click1Y; y < screen_height; y += 5) {
        line_height = little;
        if ( (y - click1Y) % 10 == 0) {
            line_height = big;
        }
        if ( (y - click1Y) % 50 == 0) {
            line_height = big + 5;
        }
        cairo_move_to(cr, C_R_BORDER_WIDTH - line_height, y + 0.5);
        cairo_line_to(cr, C_R_BORDER_WIDTH, y + 0.5);
        cairo_stroke(cr);
        // start draw numbers
        if ( (y - click1Y) % 50 == 0) {
            cairo_set_source_rgba(cr, 0, 0, 0, 0.5);
            sprintf(number, "%d", y - click1Y);
            cairo_text_extents(cr, number, &te);
            int textY = y + te.height / 2;
            textY = textY < C_R_BORDER_WIDTH + topoffset + te.height ? C_R_BORDER_WIDTH + topoffset + te.height : textY;
            cairo_move_to(cr, 5, textY);
            cairo_show_text(cr, number);
            cairo_stroke (cr);
            cairo_set_source_rgba(cr, C_R_COLOUR_TICK_R, C_R_COLOUR_TICK_G, C_R_COLOUR_TICK_B, C_R_COLOUR_TICK_A);
        }
        // end draw numbers
    }
    cairo_restore(cr);
}

static void draw_corner(cairo_t* cr)
{
    // always clear first
    cairo_save(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.0); // transparent 
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE); // prevent blending transparent to existing
    cairo_rectangle(cr, 0, topoffset, C_R_BORDER_WIDTH, C_R_BORDER_WIDTH);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_pattern_t *linpat = cairo_pattern_create_linear(
            0 + C_R_BORDER_WIDTH / 2,
            0 + topoffset + C_R_BORDER_WIDTH / 2,
            C_R_BORDER_WIDTH,
            C_R_BORDER_WIDTH + topoffset); // params define angle of gradient
    cairo_pattern_add_color_stop_rgba(linpat, 0.0, C_R_COLOUR_RULER_R, C_R_COLOUR_RULER_G, C_R_COLOUR_RULER_B, C_R_COLOUR_RULER_A);
    cairo_pattern_add_color_stop_rgba(linpat, 1.0, C_R_COLOUR_RULER_R - 0.2, C_R_COLOUR_RULER_G - 0.2, C_R_COLOUR_RULER_B - 0.2,  C_R_COLOUR_RULER_A + C_R_GRADIENT_EXTENT);
    cairo_rectangle(cr, 0, topoffset, C_R_BORDER_WIDTH, C_R_BORDER_WIDTH);
    cairo_set_source(cr, linpat);
    cairo_fill(cr);

    //cairo_set_source_rgba(cr, C_R_COLOUR_RULER_R, C_R_COLOUR_RULER_G, C_R_COLOUR_RULER_B, C_R_COLOUR_RULER_A);
    //cairo_rectangle(cr, 0, 0, C_R_BORDER_WIDTH, C_R_BORDER_WIDTH);
    //cairo_fill(cr);
    //cairo_stroke(cr);
}
static void draw_top_ruler(cairo_t* cr)
{
    cairo_pattern_t *linpat = cairo_pattern_create_linear(0, 0 + topoffset, 0, C_R_BORDER_WIDTH + topoffset); // params define angle of gradient
    cairo_pattern_add_color_stop_rgba(linpat, 0.0, C_R_COLOUR_RULER_R, C_R_COLOUR_RULER_G, C_R_COLOUR_RULER_B, C_R_COLOUR_RULER_A);
    cairo_pattern_add_color_stop_rgba(linpat, 1.0, C_R_COLOUR_RULER_R - 0.2, C_R_COLOUR_RULER_G - 0.2, C_R_COLOUR_RULER_B - 0.2, C_R_COLOUR_RULER_A + C_R_GRADIENT_EXTENT);
    cairo_rectangle(cr, C_R_BORDER_WIDTH, topoffset, screen_width, C_R_BORDER_WIDTH);
    cairo_set_source(cr, linpat);
    cairo_fill(cr);

    draw_vertical_ticks(cr);
}
static void draw_side_ruler(cairo_t* cr)
{
    cairo_pattern_t *linpat = cairo_pattern_create_linear(0, 0, C_R_BORDER_WIDTH, 0); // params define angle of gradient
    cairo_pattern_add_color_stop_rgba(linpat, 0.0, C_R_COLOUR_RULER_R, C_R_COLOUR_RULER_G, C_R_COLOUR_RULER_B, C_R_COLOUR_RULER_A);
    cairo_pattern_add_color_stop_rgba(linpat, 1.0, C_R_COLOUR_RULER_R - 0.2, C_R_COLOUR_RULER_G - 0.2, C_R_COLOUR_RULER_B - 0.2,  C_R_COLOUR_RULER_A + C_R_GRADIENT_EXTENT);
    cairo_rectangle(cr, 0, C_R_BORDER_WIDTH + topoffset, C_R_BORDER_WIDTH, screen_height);
    cairo_set_source(cr, linpat);
    cairo_fill(cr);
    draw_horizontal_ticks(cr);
}
static void clear_display_area(cairo_t* cr)
{
    cairo_save(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.0); /* transparent */
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE); // prevent blending transparent to existing
    cairo_rectangle(cr, C_R_BORDER_WIDTH, C_R_BORDER_WIDTH + topoffset, screen_width - C_R_BORDER_WIDTH, screen_height - C_R_BORDER_WIDTH);
    cairo_fill(cr);
    cairo_restore(cr);
}
static void clear_screen_area(cairo_t* cr)
{
    cairo_save(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.0); /* transparent */
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE); // prevent blending transparent to existing
    cairo_rectangle(cr, 0, 0, screen_width, screen_height);
    cairo_fill(cr);
    cairo_restore(cr);
}
static void clear_side_ruler(cairo_t* cr)
{
    cairo_save(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.0); // transparent
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE); // prevent blending transparent to existing
    cairo_rectangle(cr, 0, C_R_BORDER_WIDTH + topoffset, C_R_BORDER_WIDTH, screen_height);
    cairo_fill(cr);
    cairo_restore(cr); 
}
static void clear_top_ruler(cairo_t* cr)
{
    cairo_save(cr);
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.0); // transparent
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE); // prevent blending transparent to existing
    cairo_rectangle(cr, C_R_BORDER_WIDTH, topoffset, screen_width - C_R_BORDER_WIDTH, C_R_BORDER_WIDTH);
    cairo_fill(cr);
    cairo_restore(cr);
}


// from the examples in http://cairographics.org/samples/
static void draw_curve_box(cairo_t* cr, gdouble x0, gdouble y0, gdouble width, gdouble height)
{
    gdouble x1, y1, radius;
    //radius = 102.4;   /* and an approximate curvature radius */
    radius = 15.4;
    x1 = x0 + width;
    y1 = y0 + height;
    if (width / 2 < radius) {
        if (height / 2 < radius) {
            cairo_move_to(cr, x0, (y0 + y1)/2);
            cairo_curve_to(cr, x0 ,y0, x0, y0, (x0 + x1)/2, y0);
            cairo_curve_to(cr, x1, y0, x1, y0, x1, (y0 + y1)/2);
            cairo_curve_to(cr, x1, y1, x1, y1, (x1 + x0)/2, y1);
            cairo_curve_to(cr, x0, y1, x0, y1, x0, (y0 + y1)/2);
        } else {
            cairo_move_to(cr, x0, y0 + radius);
            cairo_curve_to(cr, x0 ,y0, x0, y0, (x0 + x1)/2, y0);
            cairo_curve_to(cr, x1, y0, x1, y0, x1, y0 + radius);
            cairo_line_to(cr, x1 , y1 - radius);
            cairo_curve_to(cr, x1, y1, x1, y1, (x1 + x0)/2, y1);
            cairo_curve_to(cr, x0, y1, x0, y1, x0, y1- radius);
        }
    } else {
        if (height / 2 < radius) {
            cairo_move_to(cr, x0, (y0 + y1)/2);
            cairo_curve_to(cr, x0 , y0, x0 , y0, x0 + radius, y0);
            cairo_line_to(cr, x1 - radius, y0);
            cairo_curve_to(cr, x1, y0, x1, y0, x1, (y0 + y1)/2);
            cairo_curve_to(cr, x1, y1, x1, y1, x1 - radius, y1);
            cairo_line_to(cr, x0 + radius, y1);
            cairo_curve_to(cr, x0, y1, x0, y1, x0, (y0 + y1)/2);
        } else {
            cairo_move_to(cr, x0, y0 + radius);
            cairo_curve_to(cr, x0 , y0, x0 , y0, x0 + radius, y0);
            cairo_line_to(cr, x1 - radius, y0);
            cairo_curve_to(cr, x1, y0, x1, y0, x1, y0 + radius);
            cairo_line_to(cr, x1 , y1 - radius);
            cairo_curve_to(cr, x1, y1, x1, y1, x1 - radius, y1);
            cairo_line_to(cr, x0 + radius, y1);
            cairo_curve_to(cr, x0, y1, x0, y1, x0, y1- radius);
        }
    }
    cairo_close_path(cr);
    
    cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, C_R_TEXT_BOX_A);
    cairo_fill(cr);
    //cairo_fill_preserve(cr);
    //cairo_set_source_rgba(cr, 0.5, 0, 0, 0.5);
    //cairo_set_line_width(cr, 10.0);
    //cairo_stroke(cr);
}

/*
 * Draw text position and make sure it gets out of the way of the cross hair
 */
static void draw_text_position(cairo_t* cr, char* text, int posX, int posY) 
{
    cairo_text_extents_t te;
    cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
                               CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, C_R_FONT_SIZE);
    cairo_text_extents (cr, text, &te);
    int x, y;
    if ( C_R_BORDER_WIDTH + 15 + te.width > posX && 
         C_R_BORDER_WIDTH + 15 + te.height + topoffset > posY) {
            // more to the right
            x = 200 > C_R_BORDER_WIDTH + 15 + te.width ? 200 : C_R_BORDER_WIDTH + 15 + te.width;
            y = C_R_BORDER_WIDTH + 5 + te.height + topoffset;
    }
    else {
            // top left
            x = C_R_BORDER_WIDTH + 5;
            y = C_R_BORDER_WIDTH + 5 + te.height + topoffset;
    }

    cairo_move_to(cr, C_R_BORDER_WIDTH + topoffset, y);
    draw_curve_box(cr,
        (gdouble)(x),
        (gdouble)(C_R_BORDER_WIDTH + topoffset + 5),
        (gdouble)(te.width + 15),
        (gdouble)(te.height + 10));
    cairo_stroke (cr);

    /* text N.B.  adding 5x5 to top left inside ruler */
    cairo_move_to(cr, x + 5, y + 5);
    cairo_set_source_rgba(cr, 0, 0, 0, 0.5);
    cairo_show_text(cr, text);

    /* white outline
    cairo_move_to(cr, x, y);
    cairo_text_path(cr, text);
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.7);
    cairo_set_line_width (cr, 1.0);
    */

    cairo_stroke (cr);

}

static void draw_text_box_size(cairo_t* cr, char* text) 
{
    cairo_text_extents_t te;
    cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
                               CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, C_R_FONT_SIZE);
    cairo_text_extents (cr, text, &te);
    /* text */
    int posX = clickBoxStartX + ((clickBoxEndX - clickBoxStartX ) / 2) - (te.width / 2);
    int posY = clickBoxStartY + ((clickBoxEndY - clickBoxStartY ) / 2) + (te.height / 2);
    // don't draw on the ruler to save having to redraw it
    posX = posX < C_R_BORDER_WIDTH + 5 ? C_R_BORDER_WIDTH + 5 : posX;
    posY = posY < C_R_BORDER_WIDTH + te.height + 5 ? C_R_BORDER_WIDTH + te.height + 5 : posY;
    cairo_move_to(cr, posX, posY);
    draw_curve_box(cr,
        (gdouble)(posX - 5),
        (gdouble)(posY - te.height - 5),
        (gdouble)(te.width + 15),
        (gdouble)(te.height + 10));

    cairo_move_to(cr, posX, posY);
    cairo_set_source_rgba(cr, 0, 0, 0, 0.5);
    cairo_show_text(cr, text);


    /* white outline
    cairo_move_to(cr, posX, posY);
    cairo_text_path(cr, text);
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.7);
    cairo_set_line_width (cr, 1.0);
    */
    cairo_stroke (cr);

}

/* This is called when we need to draw the windows contents */
static gboolean expose(GtkWidget *widget, GdkEventExpose *event, gpointer userdata)
{
    // widget->window is a GdkWindow this is first point of contact with a drawable so save it here
    drawable = widget->window;
    cairo_t *cr = gdk_cairo_create(widget->window);
    hide_cursor();
    if (supports_alpha)
        cairo_set_source_rgba (cr, 1.0, 1.0, 1.0, 0.0); /* transparent */
    else
        cairo_set_source_rgb (cr, 1.0, 1.0, 1.0); /* opaque white */

    /* draw the background */
    cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
    cairo_paint (cr);

//fprintf(stdout, "screen values on setup w=%d, h=%d\n", screen_width, screen_height);
    gdk_window_move_resize(widget->window, 0, 0, screen_width, screen_height);

    int width, height;
    gtk_window_get_size(GTK_WINDOW(widget), &width, &height);

    draw_corner(cr);
    draw_top_ruler(cr);
    draw_side_ruler(cr);
    cairo_destroy(cr);

    return FALSE;
}

/* This is called when we need to draw the windows contents */
static gboolean crosshair_expose(GtkWidget *widget, GdkEventExpose *event, gpointer userdata)
{
    // widget->window is a GdkWindow this is first point of contact with a drawable so save it here
    crosshair_drawable = widget->window;
    cairo_t *xcr = gdk_cairo_create(widget->window);
    hide_cursor();
    if (supports_alpha)
        cairo_set_source_rgba (xcr, 1.0, 1.0, 1.0, 0.0); /* transparent */
    else
        cairo_set_source_rgb (xcr, 1.0, 1.0, 1.0); /* opaque white */

    /* draw the background */
    cairo_set_operator (xcr, CAIRO_OPERATOR_SOURCE);
    cairo_paint (xcr);

fprintf(stdout, "screen values on setup w=%d, h=%d\n", screen_width, screen_height);
    gdk_window_move_resize(widget->window, 0, 0, screen_width, screen_height);

    int width, height;
    gtk_window_get_size(GTK_WINDOW(widget), &width, &height);

    cairo_destroy(cr);

    return FALSE;
}

/*
 * draws 1 pixel lines, note the 0.5 pixel shifting to make sure the line drawn
 * is 1 pixel wide this will need carefull attention when measuring sizes
 */
static void draw_horizontal_line(cairo_t *cr, int y)
{
    cairo_set_source_rgba(cr, C_R_COLOUR_LINE_R, C_R_COLOUR_LINE_G, C_R_COLOUR_LINE_B, 1.0);
    cairo_move_to(cr, 0, y + 0.5);
    cairo_line_to(cr, screen_width, y + 0.5);
    cairo_set_line_width(cr, 0.5);
    cairo_stroke(cr);
}

/* unused
static void draw_horizontal_marker(GtkWindow *window, int y)
{
    cairo_t *cr = gdk_cairo_create(drawable);
    clear_display_area(cr);
    draw_horizontal_line(cr, y);
    cairo_destroy(cr);
}
*/
static void draw_vertical_line(cairo_t *cr, int x)
{
    cairo_set_source_rgba(cr, C_R_COLOUR_LINE_R, C_R_COLOUR_LINE_G, C_R_COLOUR_LINE_B, 1.0);
    cairo_move_to(cr, x + 0.5, 0);
    cairo_line_to(cr, x + 0.5, screen_height);
    cairo_set_line_width(cr, 0.5);
    cairo_stroke(cr);
}
/* unused
static void draw_vertical_marker(GtkWindow *window, int x)
{
    cairo_t *cr = gdk_cairo_create(drawable);
    clear_display_area(cr);
    draw_vertical_line(cr, x);
    cairo_destroy(cr);
}
*/
static void draw_box(cairo_t *cr)
{
    cairo_move_to(cr, clickBoxStartX + 0.5, clickBoxStartY + 0.5);
    cairo_set_source_rgba(cr, C_R_COLOUR_BOX_R, C_R_COLOUR_BOX_G, C_R_COLOUR_BOX_B, C_R_COLOUR_BOX_A);
    cairo_rectangle(cr, clickBoxStartX + 0.5, clickBoxStartY + 0.5, clickBoxEndX - clickBoxStartX + 0.5,  clickBoxEndY - clickBoxStartY + 0.5);
    cairo_fill(cr);
    cairo_stroke(cr);
}

static void on_clicked(GtkWindow *window, GdkEventButton *event, gpointer user_data)
{
//fprintf(stdout, "button = %d\n", event->button);
    if (event->button == 3) {
        // default is "CLIPBOARD"   "PRIMARY"   is currently selected text
        //GdkAtom *atom = gdk_atom_intern("CLIPBOARD", FALSE);
        GdkAtom atom = gdk_atom_intern_static_string("CLIPBOARD");
        GtkClipboard *clipboard = gtk_clipboard_get(atom);
        char text[50];
        int ix, iy;
        ix = (int)event->x;
        iy = (int)event->y;
        sprintf( text, "%d x %d", ix - click1X, iy - click1Y);
//fprintf( stdout, "%d x %d\n", ix - click1X, iy - click1Y);
        gtk_clipboard_set_text(clipboard, text, strlen(text));
        return;
    }
    // these should exist
/*
    event->window;
    gdouble event->x;
    gdouble event_>y;
    event->state; // ctrl alt  etc
    event->button  // 12345  button number  1 and 3  are left/right
    gdouble event->x_root; // screenX
    gdouble event->y_root; // screenY
*/
    gdouble x,y;
    x = event->x;
    y = event->y;
    cairo_t *cr = gdk_cairo_create(drawable);

    if(x < C_R_BORDER_WIDTH && y < C_R_BORDER_WIDTH + topoffset) {
	// corner click
        click1X = C_R_BORDER_WIDTH;
        click1Y = C_R_BORDER_WIDTH + topoffset;
        draw_corner(cr);
        clear_side_ruler(cr);
        draw_side_ruler(cr);
        clear_top_ruler(cr);
        draw_top_ruler(cr);
    }
    else if(x < C_R_BORDER_WIDTH) {
	// side panel click
        click1Y = (int)y;
        draw_corner(cr);
        clear_side_ruler(cr);
        draw_side_ruler(cr);
        //draw_horizontal_marker(window, y);
    }
    else if(y < C_R_BORDER_WIDTH + topoffset) {
	// top panel click
        click1X = (int)x;
        draw_corner(cr);
        clear_top_ruler(cr);
        draw_top_ruler(cr);
        //draw_vertical_marker(window, x);
    }
    else {
	// screen click
        if (clickBoxStart == FALSE) {
            clickBoxStart = TRUE;
            clickBoxStartX = (int)x;
            clickBoxStartY = (int)y;
        }
        else if (clickBoxEnd == FALSE) {
            clickBoxEnd = TRUE;
            clickBoxEndX = (int)x;
            clickBoxEndY = (int)y;
            draw_box(cr);
            char text[50];

            sprintf( text, "%d x %d", clickBoxEndX - clickBoxStartX, clickBoxEndY - clickBoxStartY);
            draw_text_box_size(cr, text);
            cairo_t *cr2 = gdk_cairo_create(drawable);
            cr2 = gdk_cairo_create(crosshair_drawable);
            clear_screen_area(cr2);
            cairo_destroy(cr2);
            if (event->button == 3) {
                GdkAtom atom = gdk_atom_intern("CLIPBOARD", FALSE);
                GtkClipboard *clipboard = gtk_clipboard_get(atom);
                char text[50];
                sprintf( text, "%d x %d", clickBoxEndX - clickBoxStartX, clickBoxEndY - clickBoxStartY);
                gtk_clipboard_set_text(clipboard, text, strlen(text));
            }

        }
        else {
            clickBoxStart = FALSE;
            clickBoxEnd = FALSE;
            cairo_t *cr2 = gdk_cairo_create(drawable);
            clear_display_area(cr2);
            cairo_destroy(cr2);
        }
    }
    cairo_destroy(cr);
    /* toggle window manager frames */
    //gtk_window_set_decorated(win, !gtk_window_get_decorated(win));
}

static void on_mousemove(GtkWindow *window, GdkEventButton *event, gpointer user_data)
{

    if(clickBoxEnd == FALSE) {
        gdouble x,y;
        //x = event->x_root;
        //y = event->y_root;
        x = event->x;
        y = event->y;
    //fprintf(stdout, "move x=%d, y=%d\n", (int)x, (int)y);
        cairo_t *cr = gdk_cairo_create(crosshair_drawable);
        clear_screen_area(cr);
        draw_vertical_line(cr, x);
        draw_horizontal_line(cr, y);
        cairo_destroy(cr);

        char text[50];
        int ix, iy;
        ix = (int)x;
        iy = (int)y;
        cairo_t *cr2 = gdk_cairo_create(drawable);
        clear_display_area(cr2);
        sprintf( text, "%d x %d", ix - click1X, iy - click1Y);
        //sprintf( text, "%d x %d", ix - click1X, iy - click1Y - topoffset);
        draw_text_position(cr2, text, (int)x, (int)y);
        cairo_destroy(cr2);
    }
}

void on_keypressed(GtkWindow *window, GdkEventKey *event, gpointer user_data)
{
    // see keybindings.c
    do_keypressed(window, event, user_data);
}

GtkWidget *get_crosshair_window() {
    return xwindow;
}
GtkWidget *get_main_window() {
    return window;
}
void repaint()
{
    cairo_t *cr = gdk_cairo_create(drawable);
    clear_screen_area(cr);
    draw_corner(cr);
    draw_top_ruler(cr);
    draw_side_ruler(cr);
    cairo_destroy(cr);
}
void ruler_down()
{
    topoffset += 10;
    click1Y += 10;
    repaint();
}
void ruler_up()
{
    topoffset -= 10;
    click1Y -= 10;
    repaint();
}
void ruler_top()
{
    topoffset = 0;
    click1X = C_R_BORDER_WIDTH;
    click1Y = C_R_BORDER_WIDTH;
    repaint();
}
void show_help()
{
    //int w, h;
    cairo_surface_t *image;
    
    image = cairo_image_surface_create_from_png ("help.png");
    //w = cairo_image_surface_get_width (image);
    //h = cairo_image_surface_get_height (image);

    cairo_t *cr = gdk_cairo_create(crosshair_drawable);
    cairo_set_source_surface (cr, image, C_R_BORDER_WIDTH * 2, C_R_BORDER_WIDTH + C_R_BORDER_WIDTH + topoffset);
    cairo_paint (cr);
    cairo_surface_destroy (image);
    cairo_destroy(cr);
}
