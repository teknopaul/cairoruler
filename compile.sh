#!/bin/bash -e
#
# Compiler script, oh for make but that is a bit above me right now
#
# requires libgtk2.0-dev
#
#
cd $(dirname $0)

opts="-Wall -Werror"
#
# first compile the files with the -c flag so linking is not attmepted
# N.B. the --libs-only-L flag so we don't get -lblah attempt to link
# and a ton of warnings
#
/usr/bin/gcc $opts -c keybindings.c `pkg-config --cflags --libs-only-L gtk+-2.0`
/usr/bin/gcc $opts -c transparent.c `pkg-config --cflags --libs-only-L gtk+-2.0`
/usr/bin/gcc $opts -c cairorulercolours.c `pkg-config --cflags --libs-only-L gtk+-2.0`

# Linking is done just by specifying a list of .o output files for gcc with no flags
/usr/bin/gcc $opts transparent.o keybindings.o cairorulercolours.o -o cairoruler `pkg-config --cflags --libs gtk+-2.0` 

rm *.o

# easy when you know how, but for some reason keybindings.c can not have static functions

