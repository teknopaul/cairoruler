#!/bin/bash -e
#
# Build the .deb package
# Files to be build into the .deb should be build to ../deploy/root before calling this script (or edit it).  e.g. cp --archive src/* ../deploy/root/opt/myproj/
#
test `id -u` == "0" || (echo "Run as root" && exit 1) # requires bash -e

#
# The package name
#
NAME=cairoruler
ARCH=`uname -m`

#
# Select the files to include
#
cd `dirname $0`/..
PROJECT_ROOT=`pwd`

#
# Copy files e.g.   cp --archive src/* ${PROJECT_ROOT}/deploy/root/opt/$NAME/
#
mkdir -p ${PROJECT_ROOT}/deploy/root/usr/local/bin

cp help.png cairoruler ${PROJECT_ROOT}/deploy/root/usr/local/bin

FILES=${PROJECT_ROOT}/deploy/root

#
# Create a temporary build directory
#
TMP_DIR=/tmp/${NAME}_debbuild
rm -rf ${TMP_DIR}
mkdir -p ${TMP_DIR}
. ./version
sed -e "s/@PACKAGE_VERSION@/${VERSION}/" ${PROJECT_ROOT}/deploy/DEBIAN/control.in > ${PROJECT_ROOT}/deploy/DEBIAN/control
cp --archive -R ${FILES}/* ${TMP_DIR}/

SIZE=$(du -sk ${TMP_DIR} | cut -f 1)
sed -i -e "s/@SIZE@/${SIZE}/" ${PROJECT_ROOT}/deploy/DEBIAN/control

cp --archive -R ${PROJECT_ROOT}/deploy/DEBIAN ${TMP_DIR}/

#
# Setup the installation package ownership here if it needs root
#
#chown root.root ${TMP_DIR}/root/*
chown root.root ${TMP_DIR}/usr/local/bin/*
#chown root.root \
#  ${TMP_DIR}/root \
#  ${TMP_DIR}/home \
#  ${TMP_DIR}/usr/bin ${TMP_DIR}/usr \

#
# Build the .deb
#
dpkg-deb --build ${TMP_DIR} ${NAME}-${VERSION}-1.${ARCH}.deb

test -f ${NAME}-${VERSION}-1.${ARCH}.deb

echo "built ${NAME}-${VERSION}-1.${ARCH}.deb"
