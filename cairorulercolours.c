#include <gtk/gtk.h>
/*
    define the colurs for the components
*/
// ruler dimentions
int C_R_BORDER_WIDTH;
int C_R_TICK_BIG;
int C_R_TICK_LITTLE;
// rulers colour
gdouble C_R_COLOUR_RULER_R;
gdouble C_R_COLOUR_RULER_G;
gdouble C_R_COLOUR_RULER_B;
gdouble C_R_COLOUR_RULER_A;
gdouble C_R_GRADIENT_EXTENT;
// rulers lines
gdouble C_R_COLOUR_TICK_R;
gdouble C_R_COLOUR_TICK_G;
gdouble C_R_COLOUR_TICK_B;
gdouble C_R_COLOUR_TICK_A;
gdouble C_R_RULER_FONT_SIZE;

// marker lines
gdouble C_R_COLOUR_LINE_R;
gdouble C_R_COLOUR_LINE_G;
gdouble C_R_COLOUR_LINE_B;
gdouble C_R_COLOUR_LINE_A;

// overlay box
gdouble C_R_COLOUR_BOX_R;
gdouble C_R_COLOUR_BOX_G;
gdouble C_R_COLOUR_BOX_B;
gdouble C_R_COLOUR_BOX_A;
gdouble C_R_FONT_SIZE;

// positions boxes
gdouble C_R_TEXT_BOX_A;

void initDefaultColours();

void initDefaultColours()
{
    // ruler dimentions
    C_R_BORDER_WIDTH = 50;
    C_R_TICK_BIG = 50;
    C_R_TICK_LITTLE = 50;
    // rulers colour
    C_R_COLOUR_RULER_R = 1.0;
    C_R_COLOUR_RULER_G = 0.2;
    C_R_COLOUR_RULER_B = 0.2;
    C_R_COLOUR_RULER_A = 0.5;
    C_R_GRADIENT_EXTENT = 0.2;
    // rulers lines
    C_R_COLOUR_TICK_R = 1.0;
    C_R_COLOUR_TICK_G = 1.0;
    C_R_COLOUR_TICK_B = 1.0;
    C_R_COLOUR_TICK_A = 0.7;
    C_R_RULER_FONT_SIZE = 12.0;
    
    // marker lines
    C_R_COLOUR_LINE_R = 1.0;
    C_R_COLOUR_LINE_G = 0.2;
    C_R_COLOUR_LINE_B = 0.2;
    C_R_COLOUR_LINE_A = 0.7;
    
    // overlay box
    C_R_COLOUR_BOX_R = 1.0;
    C_R_COLOUR_BOX_G = 0.1;
    C_R_COLOUR_BOX_B = 0.1;
    C_R_COLOUR_BOX_A = 0.2;
    C_R_FONT_SIZE = 23.5;
    
    // positions boxes
    C_R_TEXT_BOX_A = 0.5;
}
