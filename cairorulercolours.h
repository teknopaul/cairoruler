/*
    define the colurs for the components
*/

#ifndef CAIRO_RULER_COLOURS_H
#define CAIRO_RULER_COLOURS_H

// ruler dimentions
int C_R_BORDER_WIDTH;
int C_R_TICK_BIG;
int C_R_TICK_LITTLE;
// rulers colour
gdouble C_R_COLOUR_RULER_R;
gdouble C_R_COLOUR_RULER_G;
gdouble C_R_COLOUR_RULER_B;
gdouble C_R_COLOUR_RULER_A;
gdouble C_R_GRADIENT_EXTENT;
// rulers lines
gdouble C_R_COLOUR_TICK_R;
gdouble C_R_COLOUR_TICK_G;
gdouble C_R_COLOUR_TICK_B;
gdouble C_R_COLOUR_TICK_A;
gdouble C_R_RULER_FONT_SIZE;

// marker lines
gdouble C_R_COLOUR_LINE_R;
gdouble C_R_COLOUR_LINE_G;
gdouble C_R_COLOUR_LINE_B;
gdouble C_R_COLOUR_LINE_A;

// overlay box
gdouble C_R_COLOUR_BOX_R;
gdouble C_R_COLOUR_BOX_G;
gdouble C_R_COLOUR_BOX_B;
gdouble C_R_COLOUR_BOX_A;
gdouble C_R_FONT_SIZE;

// positions boxes
gdouble C_R_TEXT_BOX_A;

void initDefaultColours();
#endif /* CAIRO_RULER_COLOURS_H */
