#include <gtk/gtk.h>
#include <gdk/gdkscreen.h>
#include <stdio.h>
#include <stdlib.h>
#include "transparent.h"
#include "cairorulercolours.h"

/*
void output(char* text)
{
    fprintf(stdout, text);
}
*/
void do_exit()
{
    exit(0);
}
/*
recieves
typedef struct {
  GdkEventType type;
  GdkWindow *window;
  gint8 send_event;
  guint32 time;
  guint state;
  guint keyval;
  gint length;
  gchar *string;
  guint16 hardware_keycode;
  guint8 group;
  guint is_modifier : 1;
} GdkEventKey;

F1 = help
Esc = exit
Ctrl + C = copy to clipboard as "X x Y"
Ctrl + V = move to parsing clipboard as "X x Y"
Ctrl + E = edit format
Enter = center
M = minimise
P = Pointer
X = Crosshair
C = Cursor
Up = ruler up 10
Down 0 ruler down 10
PageUp = to top
+ more opaque
- more transparent
*/
void do_keypressed(GtkWindow *win, GdkEventKey *event, gpointer user_data)
{
    //fprintf(stdout, "hardware_keycode = %d\n", event->hardware_keycode);
    //fprintf(stdout, "keyval = %d\n", event->keyval);
    if(event->hardware_keycode == 9) {
        // escape
        do_exit();
    }
    else if(event->hardware_keycode == 67) {
        // F1
        show_help();
    }
    else if(event->hardware_keycode == 36) {
        // Enter
        GtkWidget *window = get_main_window();
        GdkScreen *screen = gtk_widget_get_screen(GTK_WIDGET(window));
        screen_width = gdk_screen_get_width(screen);
        screen_height = gdk_screen_get_height(screen);
        gdk_window_move_resize(window->window, 0, 0, screen_width, screen_height);

        GtkWidget *xwindow = get_crosshair_window();
        GdkScreen *xscreen = gtk_widget_get_screen(GTK_WIDGET(xwindow));
        gdk_window_move_resize(xwindow->window, 0, 0, gdk_screen_get_width(xscreen), gdk_screen_get_height(xscreen));
        ruler_top();
    }
    else if(event->hardware_keycode == 58) {
        // M Minimize
        // keycode = 109
        // hmmm  needs two presses and then you can't get the windows back since you loose the keybindings
/*
        if( gdk_window_is_visible(event->window) ) {
            gdk_window_hide(event->window);
        }
        else {
            gdk_window_show(event->window);
        }
*/
    }
    else if(event->hardware_keycode == 33) {
        // P = Pointer
        // keycode = 112
    }
    else if(event->hardware_keycode == 53) {
        // X = Crosshair
        // keycode = 120
        GtkWidget * xwindow = get_crosshair_window();
        if( gdk_window_is_visible(xwindow->window) ) {
            gdk_window_hide(xwindow->window);
        }
        else {
            gdk_window_show(xwindow->window);
        }
    }
    else if(event->hardware_keycode == 54) {
        // C = Cursor
        // keycode = 99
    }
    else if(event->hardware_keycode == 104) {
        // Down
        ruler_down();
    }
    else if(event->hardware_keycode == 98) {
        // Up
        ruler_up();
    }
    else if(event->hardware_keycode == 99) {
        // PageUp
        ruler_top();
    }
    else if(event->hardware_keycode == 35) {
        // + more opaque
        C_R_COLOUR_RULER_A += 0.1;
        C_R_TEXT_BOX_A += 0.1;
        repaint();
    }
    else if(event->hardware_keycode == 61) {
        // - less opaque
        C_R_COLOUR_RULER_A -= 0.1;
        C_R_TEXT_BOX_A -= 0.1;
        repaint();
    }

}





