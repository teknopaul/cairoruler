#include <gtk/gtk.h>
#include <gdk/gdkscreen.h>
#include <cairo.h>
#include <stdio.h>
#include <stdlib.h>


void show_help(cairo_t* cr) {
    int w, h;
    cairo_surface_t *image;
    image = cairo_image_surface_create_from_png ("help.png");
    w = cairo_image_surface_get_width (image);
    h = cairo_image_surface_get_height (image);

    cairo_set_source_surface (cr, image, 0, 0);
    cairo_paint (cr);
    cairo_surface_destroy (image);
}